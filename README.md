# Raspcar Web Service Part

La partie Serveur développée en Node JS qui sera en relation avec la raspberry pi 4 et la base de données MongoDB

![Raspcar Logo!](raspcar.png "RaspCar Logo")

---
## Prérequis

Nous avons besoin d'installer Node js

### Node
- #### Node installation on Windows

  Just go on [official Node.js website](https://nodejs.org/) and download the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

- #### Node installation on Ubuntu

  You can install nodejs and npm easily with apt install, just run the following commands.

      $ sudo apt install nodejs
      $ sudo apt install npm
      $ npm install nodemon -g

- #### Other Operating Systems
  You can find more information about the installation on the [official Node.js website](https://nodejs.org/) and the [official NPM website](https://npmjs.org/).

Si l'installation se passe bien , exécutez ces commandes pour vérifier :

    $ node --version
    v12.13.0

    $ npm --version
    6.14.4

Pour mettre à jour npm :

    $ npm install npm -g

## Install
    $ git clone https://gitlab.com/mbds/raspcar/raspcar.git RaspcarWeb
    $ cd RaspcarWeb/server
    $ npm install
    $ sudo nodemon server.js
