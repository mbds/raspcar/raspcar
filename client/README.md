***
# Raspcar Dashboard Part

![Raspcar Logo!](../raspcar.png "RaspCar Logo")
***

## Groupe de TPT 1 - RaspCar - Participants

  -	Bah Mamadou Saliou
  
  -	Chatti Nader
  
  -	Diaz Gabriel
  
  -	Iben Salah Roukaya
  
  -	Ramoul Inès
  

# Installation
```sh
$ git clone https://gitlab.com/mbds/raspcar/raspcar.git RaspcarWeb
$ cd RaspcarWeb/client
$ npm i
$ npm start

```
