import React from 'react';
import './App.css';
import Dashboard from './component/Dashboard';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      states: {},
    };
  }

  componentDidMount() {
    this.getStatesFromServer();
    setInterval(() => {
      this.getStatesFromServer();
    }, 50);
  }

  getStatesFromServer() {
    let url = "http://localhost:8082/states";
    fetch(url)
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result)
          this.setState({
            isLoaded: true,
            states: result
          });
        },
        // Remarque : il est important de traiter les erreurs ici
        // au lieu d'utiliser un bloc catch(), pour ne pas passer à la trappe
        // des exceptions provenant de réels bugs du composant.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }
  render() {
    const { error, isLoaded, states } = this.state;
    if (error) {
      return <div>Erreur : {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Chargement…</div>;
    }
    else {
      return (
        <div className="App">
          <Dashboard
            battery={states.battery}
            moteur={states.moteur}
            brouillard={states.brouillard}
            veilleuse={states.veilleuse}
            ceinture={states.ceinture}
            huile={states.huile}
            feuCroisement={states.feuCroisement}
            freinAMain={states.freinAMain}
            pleinPhare={states.pleinPhare}
            clignottantGauche={states.clignottantGauche}
            clignottantDroite={states.clignottantDroite}
            mileage={states.mileage}
            autonomie={states.autonomie}
            acceleration={states.acceleration}
            admission={states.admission}
            distribution={states.distribution}
            temperature1={states.temperature1}
            temperature2={states.temperature2}
            temperature3={states.temperature3}
            capteursInductifs={states.capteursInductifs}
          />
        </div>
      );
    }
  }
}

export default App;
