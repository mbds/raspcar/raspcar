import React from 'react';
import { lighten, withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import { green, orange, red } from '@material-ui/core/colors';

const BorderLinearProgressGreen = withStyles({
    root: {
        height: 30,
        backgroundColor: lighten('#ffffff', 0.5),
        borderRadius: 100
    },
    bar: {
        borderRadius: 100,
        backgroundColor: green[500],
    },
})(LinearProgress);

const BorderLinearProgressOrange = withStyles({
    root: {
        height: 30,
        backgroundColor: lighten('#ffffff', 0.5),
        borderRadius: 100
    },
    bar: {
        borderRadius: 100,
        backgroundColor: orange[500],
    },
})(LinearProgress);

const BorderLinearProgressRed = withStyles({
    root: {
        height: 30,
        backgroundColor: lighten('#ffffff', 0.5),
        borderRadius: 100
    },
    bar: {
        borderRadius: 100,
        backgroundColor: red[500],
    },
})(LinearProgress);

export default function CustomizedProgressBars(props) {
    const Composant = props.autonomie <= 20 ? (<BorderLinearProgressRed
        variant="determinate"
        value={props.autonomie}
    />) : props.autonomie <= 40 ? (<BorderLinearProgressOrange
        variant="determinate"
        value={props.autonomie}
    />) : (<BorderLinearProgressGreen
        variant="determinate"
        value={props.autonomie}
    />)
    return (
        <div>
            {Composant}
        </div>
    );
}
