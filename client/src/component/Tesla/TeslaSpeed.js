import React from 'react';
//import ReactDOM from 'react-dom';
//import * as Teslahud from './teslahud.js';
import './styles/TeslaStyle.css'

class TeslaSpeed extends React.Component {
    constructor(props) {
        super(props);
        //this.state = { ctx: null };
        this.ctx = null;
        this.speedGradient = null;
        this.rpmGradient = null;
        this.myRef = React.createRef();
        this.init = this.init.bind(this);
        this.speedNeedle = this.speedNeedle.bind(this);
        this.rpmNeedle = this.rpmNeedle.bind(this);
        this.drawMiniNeedle = this.drawMiniNeedle.bind(this);
        this.calculateSpeedAngle = this.calculateSpeedAngle.bind(this);
        this.calculateRPMAngel = this.calculateRPMAngel.bind(this);
        this.drawSpeedo = this.drawSpeedo.bind(this);
    }
    componentDidMount() {
        this.init();
        console.log(this.ctx + "mount");
        var tab = [];
        var vitesseEngagee = 0;
        var regime = 0;
        setInterval(() => {
            var acceleration = this.props.acceleration;
            this.drawSpeedo(acceleration, vitesseEngagee, regime, 220);
            tab = this.simulation(acceleration, vitesseEngagee, regime);
            if(tab != null) {
                vitesseEngagee = tab[0];
                regime = tab[1];
            }
        }, 50);
    }

    init() {
        const c = this.myRef.current;
        //var c = document.createElement("canvas");
        c.width = 800;
        c.height = 800;

        var ctx = c.getContext("2d");

        //Rescale the size
        ctx.scale(1.6, 1.6);

        var speedGradient = ctx.createLinearGradient(0, 500, 0, 0);
        speedGradient.addColorStop(0, '#00b8fe');
        speedGradient.addColorStop(1, '#41dcf4');

        var rpmGradient = ctx.createLinearGradient(0, 500, 0, 0);
        rpmGradient.addColorStop(0, '#f7b733');
        rpmGradient.addColorStop(1, '#fc4a1a');
        //rpmGradient.addColorStop(1, '#EF4836');

        //console.log(ctx + "veegre")
        //return ctx;
        this.ctx = ctx;
        this.speedGradient = speedGradient;
        this.rpmGradient = rpmGradient;
    }

    simulation(vitesse, vitesseEngagee, regime) {
        if(vitesse < 220 && vitesse > 170) {
            vitesseEngagee = 6;
            regime = (vitesse / 220) / 1.25;
            return [vitesseEngagee, regime];
        }
        if(vitesse < 170 && vitesse > 120) {
            vitesseEngagee = 5;
            regime = (vitesse / 170) / 1.5;
            return [vitesseEngagee, regime];
        }
        if(vitesse < 120 && vitesse > 80) {
            vitesseEngagee = 4;
            regime = (vitesse / 120) / 1.5;
            return [vitesseEngagee, regime];
        }
        if(vitesse < 80 && vitesse > 50) {
            vitesseEngagee = 3;
            regime = (vitesse / 80) / 1.5;
            return [vitesseEngagee, regime];
        }
        if(vitesse < 50 && vitesse > 30) {
            vitesseEngagee = 2;
            regime = (vitesse / 50) / 1.75;
            return [vitesseEngagee, regime];
        }
        if(vitesse < 30) {
            vitesseEngagee = 1;
            regime = (vitesse / 30) / 1.75;
            return [vitesseEngagee, regime];
        }
        if (vitesse === 0) {
            vitesseEngagee = 0;
            regime = 0;
            return [vitesseEngagee, regime];
        }
    }

    speedNeedle(rotation) {
        var ctx = this.ctx;
        ctx.lineWidth = 2;
        ctx.save();
        ctx.translate(250, 250);
        ctx.rotate(rotation);
        ctx.strokeRect(-130 / 2 + 170, -1 / 2, 135, 1);
        ctx.restore();

        rotation += Math.PI / 180;

        this.ctx = ctx;
    }

    rpmNeedle(rotation) {
        var ctx = this.ctx;
        ctx.lineWidth = 2;
        ctx.save();
        ctx.translate(250, 250);
        ctx.rotate(rotation);
        ctx.strokeRect(-130 / 2 + 170, -1 / 2, 135, 1);
        ctx.restore();
        rotation += Math.PI / 180;

        this.ctx = ctx;
    }

    drawMiniNeedle(rotation, width, speed) {
        var ctx = this.ctx;
        ctx.lineWidth = width;
        ctx.save();
        ctx.translate(250, 250);
        ctx.rotate(rotation);
        ctx.strokeStyle = "#333";
        ctx.fillStyle = "#333";
        ctx.strokeRect(-20 / 2 + 220, -1 / 2, 20, 1);
        ctx.restore();

        let x = (250 + 180 * Math.cos(rotation));
        let y = (250 + 180 * Math.sin(rotation));

        ctx.font = "20px MuseoSans_900-webfont";
        ctx.fillText(speed, x, y);

        rotation += Math.PI / 180;
        this.ctx = ctx;
    }

    calculateSpeedAngle(x, a, b) {
        let degree = (a - b) * (x) + b;
        let radian = (degree * Math.PI) / 180;
        return radian <= 1.45 ? radian : 1.45;
    }

    calculateRPMAngel(x, a, b) {
        let degree = (a - b) * (x) + b;
        let radian = (degree * Math.PI) / 180;
        return radian >= -0.46153862656807704 ? radian : -0.46153862656807704;
    }

    drawSpeedo(speed, gear, rpm, topSpeed) {
        //this.init();
        if (speed === undefined) {
            return false;
        } else {
            speed = Math.floor(speed);
            rpm = rpm * 10;
        }
        var ctx = this.ctx;
        console.log("eefef" + ctx)
        ctx.clearRect(0, 0, 500, 500);

        ctx.beginPath();
        ctx.fillStyle = 'rgba(0, 0, 0, .9)';
        ctx.arc(250, 250, 240, 0, 2 * Math.PI);
        ctx.fill();
        ctx.save()
        ctx.restore();
        ctx.fillStyle = "#FFF";
        ctx.stroke();

        ctx.beginPath();
        ctx.strokeStyle = "#333";
        ctx.lineWidth = 10;
        ctx.arc(250, 250, 100, 0, 2 * Math.PI);
        ctx.stroke();

        ctx.beginPath();
        ctx.lineWidth = 1;
        ctx.arc(250, 250, 240, 0, 2 * Math.PI);
        ctx.stroke();

        ctx.font = "70px MuseoSans_900-webfont";
        ctx.textAlign = "center";
        ctx.fillText(speed, 250, 220);

        ctx.font = "15px MuseoSans_900-webfont";
        ctx.fillText("km/h", 250, 235);

        if (gear === 0 && speed > 0) {
            ctx.fillStyle = "#999";
            ctx.font = "70px MuseoSans_900-webfont";
            ctx.fillText('R', 250, 460);

            ctx.fillStyle = "#333";
            ctx.font = "50px MuseoSans_900-webfont";
            ctx.fillText('N', 290, 460);
        } else if (gear === 0 && speed === 0) {
            ctx.fillStyle = "#999";
            ctx.font = "70px MuseoSans_900-webfont";
            ctx.fillText('N', 250, 460);

            ctx.fillStyle = "#333";
            ctx.font = "50px MuseoSans_900-webfont";
            ctx.fillText('R', 210, 460);

            ctx.font = "50px MuseoSans_900-webfont";
            ctx.fillText(parseInt(gear) + 1, 290, 460);
        } else if (gear - 1 <= 0) {
            ctx.fillStyle = "#999";
            ctx.font = "70px MuseoSans_900-webfont";
            ctx.fillText(gear, 250, 460);

            ctx.fillStyle = "#333";
            ctx.font = "50px MuseoSans_900-webfont";
            ctx.fillText('R', 210, 460);

            ctx.font = "50px MuseoSans_900-webfont";
            ctx.fillText(parseInt(gear) + 1, 290, 460);
        } else {
            ctx.font = "70px MuseoSans_900-webfont";
            ctx.fillStyle = "#999";
            ctx.fillText(gear, 250, 460);

            ctx.font = "50px MuseoSans_900-webfont";
            ctx.fillStyle = "#333";
            ctx.fillText(gear - 1, 210, 460);
            if (parseInt(gear) + 1 < 7) {
                ctx.font = "50px MuseoSans_900-webfont";
                ctx.fillText(parseInt(gear) + 1, 290, 460);
            }
        }

        ctx.fillStyle = "#FFF";
        for (var i = 10; i <= Math.ceil(100 / 20) * 20; i += 10) {
            console.log();
            this.drawMiniNeedle(this.calculateSpeedAngle(i / 100, 83.07888, 34.3775) * Math.PI, i % 20 === 0 ? 3 : 1, i % 20 === 0 ? i : '');

            if (i <= 100) {
                this.drawMiniNeedle(this.calculateSpeedAngle(i / 47, 0, 22.9183) * Math.PI, i % 20 === 0 ? 3 : 1, i % 20 ===
                    0 ?
                    i / 10 : '');
            }
        }

        ctx.beginPath();
        ctx.strokeStyle = "#41dcf4";
        ctx.lineWidth = 25;
        ctx.shadowBlur = 20;
        ctx.shadowColor = "#00c6ff";

        ctx.strokeStyle = this.speedGradient;
        ctx.arc(250, 250, 228, .6 * Math.PI, this.calculateSpeedAngle(this.props.autonomie / 100, 83.07888, 34.3775) * Math.PI);
        ctx.stroke();
        ctx.beginPath();
        ctx.lineWidth = 25;
        ctx.strokeStyle = this.rpmGradient;
        ctx.shadowBlur = 20;
        ctx.shadowColor = "#f7b733";

        ctx.arc(250, 250, 228, .4 * Math.PI, this.calculateRPMAngel(rpm / 4.7, 0, 22.9183) * Math.PI, true);
        ctx.stroke();
        ctx.shadowBlur = 0;


        ctx.strokeStyle = '#41dcf4';
        //this.speedNeedle(this.calculateSpeedAngle(speed / topSpeed, 83.07888, 34.3775) * Math.PI);

        ctx.strokeStyle = this.rpmGradient;
        //this.rpmNeedle(this.calculateRPMAngel(rpm / 4.7, 0, 22.9183) * Math.PI);

        ctx.strokeStyle = "#000";

        this.ctx = ctx;
    }

    render() {
        //let canvasHTML = Teslahud.drawSpeedo(120, 4, .8, 160);

        return (
            <canvas ref={this.myRef}></canvas>
        );
    }
}

export default TeslaSpeed