import React from 'react';
import Grid from '@material-ui/core/Grid';
import { loadCSS } from 'fg-loadcss';
import Icon from '@material-ui/core/Icon';
import { green, grey, red } from '@material-ui/core/colors';

import BatteryOff from './../images/batterie - 33x33px.bmp';
import BatteryOn from './../images/batterie - éclairage - 33x33px.bmp';
import moteurOff from './../images/moteur - 33x33px.bmp';
import moteurOn from './../images/moteur - éclairage -33x33px.bmp';
import BrouillardOff from './../images/brouillard - 33x33px.bmp';
import BrouillardOn from './../images/brouillard - éclairage - 33x33px.bmp';
import CeintureOff from './../images/ceinture - 33x33px.bmp';
import CeintureOn from './../images/ceinture - éclairage- 33x33px.bmp';
import FeuCroisementOff from './../images/feux de croisement - 33x33px.bmp';
import FeuCroisementOn from './../images/feux de croisement - éclairage - 33x33px.bmp';
import FreinAMainOff from './../images/frein à main - 33x33px.bmp';
import FreinAMainOn from './../images/frein à main - éclairage - 33x33px.bmp';
import HuileOff from './../images/huile - 33x33px.bmp';
import HuileOn from './../images/huile - éclairage - 33x33px.bmp';
import PleinPhareOff from './../images/plein phare - 33x33px.bmp';
import PleinPhareOn from './../images/plein phare - éclairage - 33x33px.bmp';
import VeilleuseOff from './../images/veilleuse - 33x33px.bmp';
import VeilleuseOn from './../images/veilleuse - éclairage - 33x33px.bmp';
import ImageBas from './../images/ImageBas.png';
import ImageGauche from './../images/ImageGauche.jpg';
import ImageDroite from './../images/ImageDroite.jpg'
import AirPod from './../images/pod driagnostic vert.png';
import './../styles/styles.css';
import { Box } from '@material-ui/core';
import TeslaSpeed from './Tesla/TeslaSpeed';
//import ProgressBar from './Tesla/ProgressBar';

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = { date: new Date() };
    }
    componentDidMount() {
        loadCSS(
            'https://use.fontawesome.com/releases/v5.12.0/css/all.css',
            document.querySelector('#font-awesome-css'),
        );
        setInterval(() => {
            this.setState({
                date: new Date()
            });
        }, 5000);
    }
    render() {
        const battery = (this.props.battery === "1") ? BatteryOn : BatteryOff;
        const moteur = (this.props.moteur === "1") ? moteurOn : moteurOff;
        const brouillard = (this.props.brouillard === "1") ? BrouillardOn : BrouillardOff;
        const ceinture = (this.props.ceinture === "1") ? CeintureOn : CeintureOff;
        const feuCroisement = (this.props.feuCroisement === "1") ? FeuCroisementOn : FeuCroisementOff;
        const freinAMain = (this.props.freinAMain === "1") ? FreinAMainOn : FreinAMainOff;
        const huile = (this.props.huile === "1") ? HuileOn : HuileOff;
        const pleinPhare = (this.props.pleinPhare === "1") ? PleinPhareOn : PleinPhareOff;
        const veilleuse = (this.props.veilleuse === "1") ? VeilleuseOn : VeilleuseOff;
        const clignottantGaucheColor = (this.props.clignottantGauche === "1") ? { color: green[500], fontSize: 70 } : { color: grey[500], fontSize: 70 };
        const clignottantDroiteColor = (this.props.clignottantDroite === "1") ? { color: green[500], fontSize: 70 } : { color: grey[500], fontSize: 70 };
        const temperature = (this.props.temperature1 <= 100) ? (<Icon style={{ fontSize: 50, color: grey[500] }} className="fas fa-temperature-low" />) : <Icon style={{ fontSize: 50, color: red[500] }} className="fas fa-temperature-high" />
        return (
            <div>
                <Grid container>
                    <Grid
                        container
                        direction="row"
                        justify="space-between"
                        alignItems="flex-start"
                        style={{ fontSize: 50 }}
                    >
                        <Grid item xs={1}></Grid>
                        <Grid item xs={3}>
                            <Box textAlign="left">
                                <Icon style={clignottantGaucheColor} className="fa fa-long-arrow-alt-left" />
                            </Box>
                        </Grid>
                        <Grid item xs={4}>
                            <Box textAlign="center">
                                {this.state.date.getHours()}:{this.state.date.getMinutes()}
                            </Box>
                        </Grid>
                        <Grid item xs={3}>
                            <Box textAlign="right">
                                <Icon style={clignottantDroiteColor} className="fa fa-long-arrow-alt-right" />
                            </Box>
                        </Grid>
                        <Grid item xs={1}></Grid>
                    </Grid>
                    <Grid container>
                        <Grid item xs={3}>
                            <img alt="gauche" src={ImageGauche} style={{ height: '75%', marginTop: '23%' }} />
                            <Grid container style={{ marginTop: '-20%', fontSize: '150%' }}>
                                <Grid item xs={3}>5 km</Grid>
                                <Grid item xs={6}></Grid>
                                <Grid item xs={3}>{this.props.mileage} km</Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={6}>
                            <TeslaSpeed
                                acceleration={this.props.acceleration}
                                autonomie={this.props.autonomie}
                            />
                        </Grid>
                        <Grid item xs={3}>
                            <img alt="droite" src={ImageDroite} style={{ height: '75%', marginTop: '23%', marginLeft: '-115%' }} />
                        </Grid>
                        <Grid container justify="center" style={{ marginTop: '-5px' }}>
                            <Grid item xs={6}>
                                {temperature}
                            </Grid>
                        </Grid>
                        <Grid container justify="center">
                            <Grid item xs={6} style={{ fontSize: '32px' }}>
                                {this.props.temperature1}°
                            </Grid>
                        </Grid>
                        <Grid container justify="center" spacing={0} style={{ position: "relative" }}>
                            <img alt="Bas" src={ImageBas} style={{ width: '100%' }} />
                            <span><img alt="battery" src={battery} className="mesIcons" /></span>
                            <span><img alt="moteur" src={moteur} className="mesIcons" /></span>
                            <span><img alt="huile" src={huile} className="mesIcons" /></span>
                            
                            <span><img alt="ceinture" src={ceinture} className="mesIcons" /></span>
                            <span><img alt="bfreinAMainattery" src={freinAMain} className="mesIcons" /></span>
                            <span><img alt="veilleuse" src={veilleuse} className="mesIcons" /></span>
                            <span><img alt="feuCroisement" src={feuCroisement} className="mesIcons" /></span>
                            <span><img alt="pleinPhare" src={pleinPhare} className="mesIcons" /></span>
                            <span><img alt="brouillard" src={brouillard} className="mesIcons" /></span>
                            <img alt="airPod" src={AirPod} className="airPod" />
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default Dashboard;