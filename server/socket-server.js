var app = require('express')();
var fs = require('fs');
var server = require('http').createServer(app);
var io = require('socket.io')(server);

var CarPosition = require('./model/car-position');
var carDataProvider = require('./utils/car-data.provider');


app.get('/', function (req, res) {
    fs.readFile(__dirname + '/public/index.html', function(err, data) {
        if (err) {
            res.writeHead(404, {'Content-Type': 'text/html'});
            return res.end("404 Not Found");
        }
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(data);
        return res.end();
    });
});


io.on('connection', function(socket){
    socket.on('NewConnectedDevice', function(deviceName) {
        console.log("A new device is connnected:", deviceName);
    });

   // tourner le moteur de la voiture:
    socket.on('state_start', function(state_start) {
        console.log("tourner la voiture:", state_start);
    });
    //Eteindre le moteur de la  voiture
    socket.on('state_start', function(state_start) {
        console.log("Eteindre la  voiture:", state_start);
    });

    setInterval(function(){
        var pos = carDataProvider.getCarPosition();
        socket.emit("CarPosition", pos.longitude + "," + pos.latitude);
    }, 1000);
});

server.listen(3000);



