const express = require('express');
var MongoClient = require('mongodb').MongoClient;
var rpio = require('rpio');
const app = express();
const port = 8082;

var fs = require('fs');
var server = require('http').createServer(app);
var io = require('socket.io')(server);

var CarPosition = require('./model/car-position');
var carDataProvider = require('./utils/car-data.provider');

const bodyParser = require('body-parser');

rpio.spiBegin();
var Gpio = require('onoff').Gpio;
var pushButton1 = new Gpio(5, 'in', 'both');
var pushButton2 = new Gpio(6, 'in', 'both');
var pushButton3 = new Gpio(13, 'in', 'both');
var pushButton4 = new Gpio(19, 'in', 'both');
var pushButton5 = new Gpio(26, 'in', 'both');

var voyants = {
    cltGauche: false,
    cltDroite: false,
    ceinture: true,
    freinAMain: true,
    feux: false
}

pushButton1.watch(function (err, value) {
    if(value === 0) {
        voyants.cltGauche = !voyants.cltGauche;
        if(voyants.cltGauche && voyants.cltDroite) {
            voyants.cltDroite = !voyants.cltDroite;
        }
    }
  });

  pushButton2.watch(function (err, value) {
    if(value === 0) {
        voyants.cltDroite = !voyants.cltDroite;
        if(voyants.cltDroite && voyants.cltGauche) {
            voyants.cltGauche = !voyants.cltGauche;
        }
    }
  });
  
  pushButton3.watch(function (err, value) {
    if(value === 0) {
        voyants.ceinture = !voyants.ceinture;
    }
  });

  pushButton4.watch(function (err, value) {
    if(value === 0) {
        voyants.freinAMain = !voyants.freinAMain;
    }
  });

  pushButton5.watch(function (err, value) {
    if(value === 0) {
        voyants.feux = !voyants.feux;
    }
  });

var multer = require('multer');
var multerData = multer();

var states = {
    battery: "0",
    moteur: "0",
    brouillard: "0",
    veilleuse: "0",
    ceinture: "0",
    huile: "0",
    feuCroisement: "0",
    freinAMain: "0",
    pleinPhare: "0",
    clignottantGauche: "0",
    clignottantDroite: "0",
    mileage: "1234",
    acceleration: 0,
    autonomie: 0,
    admission: 0,
    distribution: 0,
    temperature1: 0,
    temperature2: 0,
    temperature3: 0,
    capteursInductifs: 0
}

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE");

    next();
});

app.get('/', function (req, res) {
    fs.readFile(__dirname + '/public/index.html', function(err, data) {
        if (err) {
            res.writeHead(404, {'Content-Type': 'text/html'});
            return res.end("404 Not Found");
        }
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(data);
        return res.end();
    });
});


io.on('connection', function(socket){
    socket.on('NewConnectedDevice', function(deviceName) {
        console.log("A new device is connnected:", deviceName);
    });

   // tourner le moteur de la voiture:
    socket.on('state_start', function(state_start) {
        console.log("tourner la voiture:", state_start);
    });
    //Eteindre le moteur de la  voiture
    socket.on('state_start', function(state_start) {
        console.log("Eteindre la  voiture:", state_start);
    });

    setInterval(function(){
        var pos = carDataProvider.getCarPosition();
        socket.emit("CarPosition", pos.longitude + "," + pos.latitude);
    }, 1000);
});

app.get('/states', function (req, res) {
    states = updateStates();
    res.send(states);
})

app.listen(port, function () {
    console.log('Server listening on port ', port)
})

function getSignals() {
    var arr = [];
    for (var channel = 0; channel < 8; channel++) {
        // Preparation du TX buffer [trigger byte = 0x01] [channel 0 = 0x80 (128)] [placeholder = 0x01]
        var txBuffer = new Buffer([0x01, (8 + channel << 4), 0x01]);
        var rxBuffer = new Buffer(txBuffer.byteLength);

        // Envoie du TX buffer et réception du RX buffer
        rpio.spiTransfer(txBuffer, rxBuffer, txBuffer.length);

        // Extraction de la valeure à partir de l'output buffer. Ignore le premier byte.
        var junk = rxBuffer[0],
            MSB = rxBuffer[1],
            LSB = rxBuffer[2];

        // Ignore les six premiers bits du MSB, décalage de bits MSB 8 positions et
        // enfin combiner LSB et MSB pour obtenir une valeur complète de 10 bits
        var value = ((MSB & 3) << 8) + LSB;
        arr[channel] = value
    };
    return arr;
}

function updateStates() {
    var arr = getSignals();
    states.acceleration = Math.round((((arr[0] * 100) / 1023) * 220) / 100);
    states.autonomie = Math.round((arr[1] * 100) / 1023);
    states.admission = Math.round((arr[2] * 100) / 1023);
    states.distribution = Math.round((arr[3] * 100) / 1023);
    states.temperature1 = Math.round((((arr[4] * 100) / 1023) * 130) / 100);
    states.temperature2 = Math.round((arr[5] * 100) / 1023);
    //states.temperature3 = Math.round((arr[6] * 100) / 1023);
    // states.capteursInductifs = Math.round((arr[7] * 100) / 1023);

    states.moteur = (states.admission < 30 || states.admission > 70) ? '1' : '0';
    states.huile = (states.distribution < 30 || states.distribution > 70) ? '1' : '0';
    states.battery = (states.temperature2 < 40) ? '1' : '0';

    states.clignottantGauche = (voyants.cltGauche === true) ? '1' : '0';
    states.clignottantDroite = (voyants.cltDroite === true) ? '1' : '0';
    states.ceinture = (voyants.ceinture === true) ? '1' : '0';
    states.freinAMain = (voyants.freinAMain === true) ? '1' : '0';
    
    states.veilleuse = (voyants.feux === true) ? '1' : '0';
    states.feuCroisement = (voyants.feux === true) ? '1' : '0';
    states.pleinPhare = (voyants.feux === true) ? '1' : '0';
    states.brouillard = (voyants.feux === true) ? '1' : '0';

    return states;
}

// Modification user
app.post('/update/user', multerData.fields([]), function (req, res) {
    MongoClient.connect("mongodb://raspcar:raspcar2020@ds247001.mlab.com:47001/raspcar", function (err, client) {

        if (err) throw err;
        var db = client.db("raspcar");
        db.collection('user', function (err, collection) {

            var userName = req.body.userName;
            var password = req.body.password;

            collection.updateOne({ "userName": userName }, { $set: { "password": password } }, function (err, user) {
                if (err) throw err;
                let response;
                if (!err) {
                    //console.log(user);
                    response = {
                        succes: true,
                        error: false,
                        msg: "l'utilsateur a bien été modifié"
                    }
                }
                else {
                    //console.log("erreur")
                    response = {
                        msg: "Une erreur est apparue",
                        error: true,
                        succes: false
                    }
                }
                res.send(JSON.stringify(response));
                console.log(response);
            });
        });
    });
});



// Connexion avec login et mot de passe
app.post('/login', multerData.fields([]), function (req, res) {
    MongoClient.connect("mongodb://raspcar:raspcar2020@ds247001.mlab.com:47001/raspcar", function (err, client) {

        if (err) throw err;
        var db = client.db("raspcar");
        db.collection('user', function (err, collection) {

            var userName = req.body.userName;
            var password = req.body.password;

            collection.find({ "userName": userName, "password": password }).toArray(function (err, user) {
                if (err) throw err;
                let response;
                if (user.length > 0) {
                    //console.log(user);
                    response = {
                        succes: true,
                        error: false,
                        msg: "l'utilsateur existe belle et bien",
                        data: user,
                    }
                }
                else {
                    //console.log("erreur")
                    response = {
                        msg: "l'utilsateur n'existe pas",
                        error: true,
                        succes: false
                    }
                }
                res.send(JSON.stringify(response));
                console.log(response);
            });
        });
    });
});


// Création de profil 
app.post('/register', multerData.fields([]), function (req, res) {
    MongoClient.connect("mongodb://raspcar:raspcar2020@ds247001.mlab.com:47001/raspcar", function (err, client) {

        if (err) throw err;
        var db = client.db("raspcar");

        db.collection('user', function (err, collection) {

            let userToInsert = {
                userName: req.body.userName,
                password: req.body.password,
                //nom: req.body.nom,
                //prenom: req.body.prenom
            };

            collection.find({ "userName": userToInsert.userName }).toArray(function (err, user) {
                if (err) throw err;
                let response;
                if (user.length > 0) {
                    //console.log("erreur")
                    // !!!!!!!!!!!!!! NE SURTOUT PAS MODIFIER LE MESSAGE D'ERREUR !!!!!!!!!!!!!!
                    response = {
                        error: true,
                        succes: false,
                        msg: "l'utilsateur existe déjà"
                    }
                    res.send(JSON.stringify(response));
                    console.log(response);
                }
                else {
                    db.collection("user")
                        .insertOne(userToInsert, function (err, insertedId) {
                            if (!err) {
                                response = {
                                    succes: true,
                                    error: false,
                                    msg: "Ajout réussi"
                                }
                            } else {
                                response = {
                                    succes: false,
                                    error: err,
                                    msg: "Problème à l'insertion"
                                }
                            }
                            res.send(JSON.stringify(response));
                            console.log(response);
                        })
                }

            });
        });

    });
});


// Insertion des données statistiques
app.post('/stats', multerData.fields([]), function (req, res) {
    MongoClient.connect("mongodb://raspcar:raspcar2020@ds247001.mlab.com:47001/raspcar", function (err, client) {

        if (err) throw err;
        var db = client.db("raspcar");

        let userToInsert = {
            date: new Date(),
            mileage: req.body.mileage,
            battery: req.body.battery
        };
        db.collection("carinfo")
            .insertOne(userToInsert, function (err, insertedId) {
                let response;
                if (!err) {
                    response = {
                        succes: true,
                        result: insertedId.ops[0]._id,
                        error: null,
                        msg: "Ajout réussi " + JSON.stringify(userToInsert) + "---" + insertedId.ops[0]._id
                    };
                } else {
                    response = {
                        succes: false,
                        error: err,
                        msg: "Problème à l'insertion"
                    };
                }
                res.send(response)
            });
    });
});


// Récupération des statistiques
app.get('/stats', multerData.fields([]), function (req, res) {
    MongoClient.connect("mongodb://raspcar:raspcar2020@ds247001.mlab.com:47001/raspcar", function (err, client) {

        if (err) throw err;
        var db = client.db("raspcar");
        db.collection('carinfo', function (err, collection) {

            collection.find().toArray(function (err, user) {
                if (err) throw err;
                let response;
                if (user.length > 0) {
                    //console.log(user);
                    response = {
                        succes: true,
                        error: false,
                        msg: "Les stats ont été bien téléchargées",
                        data: user,
                    }
                }
                else {
                    //console.log("erreur")
                    response = {
                        msg: "Pas de stats",
                        error: true,
                        succes: false
                    }
                }
                res.send(JSON.stringify(response));
            });
        });
    });
});
