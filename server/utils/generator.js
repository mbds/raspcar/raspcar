const CarPosition = require('../model/car-position');

module.exports = {
    generatePostion: function () {
        var longitude = 43.6221258;
        var latitude = 7.0390913;
        return new CarPosition(longitude, latitude)
    }
};